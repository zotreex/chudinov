package com.example.tinkoff_lab.api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


object ApiService {
	const val baseUrl = "https://developerslife.ru"

	fun retrofitService(): Api {
		return Retrofit.Builder()
			.baseUrl(baseUrl)
			.addConverterFactory(GsonConverterFactory.create())
			.build()
			.create(Api::class.java)
	}
}