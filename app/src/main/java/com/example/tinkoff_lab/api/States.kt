package com.example.tinkoff_lab.api

data class States<out T>(val status: Status, val data: T?, val error: String?) {

	companion object {
		fun <T> success(data: T?): States<T> {
			return States(Status.SUCCESS, data, null)
		}

		fun <T> error(error: String?): States<T> {
			return States(Status.ERROR, null, error)
		}
	}
}

enum class Status {
	SUCCESS,
	ERROR
}