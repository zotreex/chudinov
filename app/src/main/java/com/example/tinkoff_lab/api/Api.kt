package com.example.tinkoff_lab.api

import com.example.tinkoff_lab.data.Post
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface Api {
	@GET("/random?json=true")
	suspend fun getPosts(): Response<Post>
}