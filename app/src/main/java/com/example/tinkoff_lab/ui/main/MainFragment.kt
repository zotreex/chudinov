package com.example.tinkoff_lab.ui.main

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.gif.GifDrawable
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.example.tinkoff_lab.R
import com.example.tinkoff_lab.api.Status
import com.example.tinkoff_lab.data.Post
import com.example.tinkoff_lab.databinding.MainFragmentBinding
import com.google.android.material.snackbar.Snackbar

class MainFragment : Fragment() {

	companion object {
		fun newInstance() = MainFragment()
	}

	private lateinit var viewModel: MainViewModel
	private lateinit var binding: MainFragmentBinding

	override fun onCreateView(
		inflater: LayoutInflater,
		container: ViewGroup?,
		savedInstanceState: Bundle?
	): View? {
		binding = MainFragmentBinding.inflate(inflater, container, false)
		return binding.root
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)
		viewModel = ViewModelProvider(this).get(MainViewModel::class.java)

		observePost()
		observeBack()
		observeLoading()

		binding.next.setOnClickListener { viewModel.onNextClick() }
		binding.back.setOnClickListener { viewModel.onBackClick() }
	}

	private fun observePost() {
		viewModel.posts.observe(viewLifecycleOwner, Observer {
			when (it.status) {
				Status.SUCCESS -> {
					binding.next.isEnabled = true
					it.data?.let { showCard(it) }
				}
				Status.ERROR -> {
					binding.next.isEnabled = false
					it.error?.let { showError(it) }
				}
			}
		})
	}

	private fun observeBack() {
		viewModel.backStatus.observe(viewLifecycleOwner, Observer {
			binding.back.isEnabled = it
		})
	}

	private fun observeLoading() {
		viewModel.loading.observe(viewLifecycleOwner, Observer {
			if (it) {
				with(binding) {
					progress.visibility = View.VISIBLE
					next.isEnabled = false
				}
			} else {
				binding.progress.visibility = View.GONE
			}
		})
	}

	private fun showCard(post: Post) {
		binding.description.text = post?.description
		Glide
			.with(requireContext())
			.asGif()
			.centerCrop()
			.listener(glideListener)
			.diskCacheStrategy(DiskCacheStrategy.ALL)
			.load(post.gifURL)
			.into(binding.gifView)
	}

	private fun showError(error: String) {
		Snackbar.make(
			binding.main,
			error,
			Snackbar.LENGTH_INDEFINITE
		).setAction(getString(R.string.update)) {
			viewModel.getNewPost()
		}.show()
	}

	private val glideListener = object : RequestListener<GifDrawable> {
		override fun onLoadFailed(
			e: GlideException?,
			model: Any?,
			target: Target<GifDrawable>?,
			isFirstResource: Boolean
		): Boolean {
			binding.progress.visibility = View.GONE
			return false
		}

		override fun onResourceReady(
			resource: GifDrawable?,
			model: Any?,
			target: Target<GifDrawable>?,
			dataSource: DataSource?,
			isFirstResource: Boolean
		): Boolean {
			viewModel.onGlideFinished()
			return false
		}
	}
}