package com.example.tinkoff_lab.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.tinkoff_lab.api.Api
import com.example.tinkoff_lab.api.ApiService
import com.example.tinkoff_lab.api.States
import com.example.tinkoff_lab.data.Post
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.lang.Exception

class MainViewModel : ViewModel() {
	private var api: Api = ApiService.retrofitService()

	private var history: ArrayList<Post> = arrayListOf()
	private var currentPosition = 0

	private val _post = MutableLiveData<States<Post>>()
	val posts = _post as LiveData<States<Post>>

	private val _loading = MutableLiveData<Boolean>()
	val loading = _loading as LiveData<Boolean>

	private val _backStatus = MutableLiveData<Boolean>(false)
	val backStatus = _backStatus as LiveData<Boolean>

	init {
		getNewPost()
	}

	fun onGlideFinished() {
		_loading.postValue(false)
	}

	fun onNextClick() {
		if (currentPosition == history.size - 1) {
			getNewPost()
		} else {
			_post.postValue(States.success(history[currentPosition + 1]))
		}
		currentPosition++

		checkBack()
	}

	fun onBackClick() {
		currentPosition--
		_post.postValue(States.success(history[currentPosition]))

		checkBack()
	}

	private fun checkBack() {
		if (history.size > 0 && currentPosition != 0) _backStatus.postValue(true)
		else _backStatus.postValue(false)
	}

	fun getNewPost() {
		viewModelScope.launch(Dispatchers.IO) {
			_loading.postValue(true)
			try {
				val result = api.getPosts()

				result.body()?.let {
					history.add(it)
					_post.postValue(States.success(it))
					return@launch
				}

			} catch (e: Exception) {
				_post.postValue(States.error("Произошла ошибка"))
				_loading.postValue(false)
			}
		}
	}
}